
package com.christianfokoua.almacenarjuegos;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author chris
 */
public class GestionXml {
    
 private  DocumentBuilderFactory factory;     
 private DocumentBuilder dBuilder;
 

 private Document document;
 
 private String stringRoot;
 private String nombreFicheroXml;
 private ArrayList<Juego> juegosAlmacenadas;

 
 private Source source;
 private Result result;
 private Transformer transformer;
 private Element root;
 

    public GestionXml(String nombreFichero, String raizXml) {
     try {
         stringRoot = raizXml;
         nombreFicheroXml = nombreFichero;
         
         factory = DocumentBuilderFactory.newInstance();
         dBuilder = factory.newDocumentBuilder();
         document = dBuilder.newDocument();
         
         //raiz
         root = document.createElement(stringRoot);
         document.appendChild(root);
         
     } catch (ParserConfigurationException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
    
    public Element addElement(String stringElement){
        
        Element eHijo = document.createElement(stringElement);
        root.appendChild(eHijo);
        return eHijo;
    }
    
    public Element addElement(String stringElement, Element ePadre){
        
       Element eHijo = document.createElement(stringElement);
       ePadre.appendChild(eHijo); 
       return eHijo;
    }
    
    public void addAttribut(String stringAttr, String valor , Element eAttr){
       
        Attr attr = document.createAttribute(stringAttr);
        attr.setValue(valor);
        eAttr.setAttributeNode(attr);
        
    }
 
    public void addValue(String valor, Element e , Element ePadre){
        e.appendChild(document.createTextNode(valor));
        ePadre.appendChild(e);
    }
    
    public void xFormerXml(){
     try {
         source = new DOMSource(document);
         
         result = new StreamResult(new File(nombreFicheroXml));
         transformer = TransformerFactory.newInstance().newTransformer();
         transformer.transform(source, result);
//         System.out.println("Xformacion realizado con exit!");
     } catch (TransformerConfigurationException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     } catch (TransformerException ex) {
         Logger.getLogger(GestionXml.class.getName()).log(Level.SEVERE, null, ex);
     }
    }
 
    public void cargarJuego(String nombreFichero){
        
                 
       
        
        File file = new File(nombreFichero);
        try {
  DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
  DocumentBuilder builder = dbFactory.newDocumentBuilder();
  Document doc = builder.parse(file);
  juegosAlmacenadas = new ArrayList<>();
  doc.getDocumentElement().normalize();
  
  // almacenamos los nodos para luego mostrar la
// cantidad de ellos con el método getLength()
NodeList nList = doc.getElementsByTagName("juego");
System.out.println("Número de juegos: " + nList.getLength()+"\n");

for(int temp = 0; temp < nList.getLength(); temp++) {
  Node nNode = nList.item(temp);

  if(nNode.getNodeType() == Node.ELEMENT_NODE) {
    Element eElement = (Element) nNode;

    
    System.out.println("Titulo: "
                + eElement.getElementsByTagName("titulo").item(0).getTextContent());
    
   String str1 = eElement.getElementsByTagName("titulo").item(0).getTextContent();
    
    System.out.println("Genero: "
                + eElement.getElementsByTagName("genero").item(0).getTextContent());
    
   String str2 = eElement.getElementsByTagName("genero").item(0).getTextContent();
     
    System.out.println("Plataforma: "
                + eElement.getElementsByTagName("plataforma").item(0).getTextContent());
    
     String str3 = eElement.getElementsByTagName("plataforma").item(0).getTextContent();
     
    System.out.println("Fecha de Publicación: "
                + eElement.getElementsByTagName("fecha").item(0).getTextContent());
    
    String str4 = eElement.getElementsByTagName("fecha").item(0).getTextContent();
    
      System.out.println("");

      Juego newJuego = new Juego(str1, str2, str3, Integer.parseInt(str4));
      juegosAlmacenadas.add(newJuego);
  }
}

         


} catch(IOException | NumberFormatException | ParserConfigurationException | DOMException | SAXException e) {
  e.printStackTrace();
}
        

       
    }
    
    public void creearFicheroXml(GestionXml gst){
                for (int i = 0; i < getJuegosAlmacenadas().size(); i++) {
               
            
            
    Element juego = gst.addElement("juego");
    
    Element titulo = gst.addElement("titulo", juego);
    gst.addValue(getJuegosAlmacenadas().get(i).getTitulo(),titulo , juego);
    
    Element genero = gst.addElement("genero", juego);
    gst.addValue(getJuegosAlmacenadas().get(i).getGenero(),genero , juego);
    
    Element plataforma = gst.addElement("plataforma", juego);
    gst.addValue(getJuegosAlmacenadas().get(i).getPlataforma(),plataforma , juego);
    
    Element fecha = gst.addElement("fecha", juego);
    gst.addValue(Integer.toString(getJuegosAlmacenadas().get(i).getFechaLanzamiento()),fecha , juego); 
        }
    }

    
    
    public  boolean isInt(String chaine){
boolean valeur = true;
char[] tab = chaine.toCharArray();

for(char carac : tab){
if(!Character.isDigit(carac) && valeur){ valeur = false; }
}

return valeur;
}
   
    public  int menuSelect(){
        int select = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("===========MENU============"
                            +"\n 1- Añadir Juego "
                            +"\n 2- Eliminar Juego"
                            +"\n 3- Salir");
       
        System.out.println("\n\n Entra un numero de seleccion : ");
        String str = sc.nextLine();
             
        Boolean b = isInt(str);
        
        if(b){
            
            if(Integer.parseInt(str)  > 0 && Integer.parseInt(str) < 4){
                switch (Integer.parseInt(str)) {
                    case 1:
                        System.out.println("Selección 1 \n");
                        select= 1;
                        break;
                    case 2:
                        System.out.println("Selección 2 \n");
                        select = 2;
                        break;
                    default:
                        System.out.println("Selección 3 \n");
                        select = 3;
                        break;
                }
            }else{
                System.err.println("Selección incorrecta ! \n");
                menuSelect();
            }
            
        }else{
            System.err.println("Lo que has puesto no es un numero porfavor ententa otra vez \n"); 
            menuSelect();
        }
       
        return select;
    }

    /**
     * @return the juegosAlmacenadas
     */
    public ArrayList<Juego> getJuegosAlmacenadas() {
        return juegosAlmacenadas;
    }

    /**
     * @param juegosAlmacenadas the juegosAlmacenadas to set
     */
    public void setJuegosAlmacenadas(ArrayList<Juego> juegosAlmacenadas) {
        this.juegosAlmacenadas = juegosAlmacenadas;
    }

     
    

             
        
      
           
       
    
    
}
