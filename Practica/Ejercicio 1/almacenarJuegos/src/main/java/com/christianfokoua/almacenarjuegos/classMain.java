package com.christianfokoua.almacenarjuegos;

import java.util.ArrayList;
import java.util.Scanner;
import org.w3c.dom.Element;




/**
 *
 * @author willy christian fokoua
 */
public class classMain {
    
    public static void main(String[] args){
        
        String nombreFichero = "Juego.xml";
  
        GestionXml gst = new GestionXml(nombreFichero, "listadejuegos");
       
        gst.cargarJuego(nombreFichero);
        
        int select = gst.menuSelect();
        
        Scanner sc = new Scanner(System.in);
        
        switch (select) {
            case 1:
                {
                    System.out.println("Entra un titulo de juego :");
                    String titulo = sc.nextLine();
                    System.out.println("Entra el genero de este juego :");
                    String genero = sc.nextLine();
                    System.out.println("Entra la plataforma de este juego :");
                    String plataforma =  sc.nextLine();
                    System.out.println("Entra la fecha de lanzamiento de este juego :");
                    String fecha =sc.nextLine();
                    Juego newJuego = new Juego(titulo, genero, plataforma, Integer.parseInt(fecha));
                    gst.getJuegosAlmacenadas().add(newJuego);
                    System.out.println("Nuevo juego creada con exit ! \n\n");
                    
                    gst.creearFicheroXml(gst); //linea que crea el fichero xml
                    gst.xFormerXml(); //linea que transforma y almacena el fichero xml
                    System.out.println("\n\n Programa Finalizado!");
                    break;
                }
        //---------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------   

            case 2:
                {
                    System.out.println("LISTA DE JUEGO DISPONIBLE");
                    for (int i = 0; i < gst.getJuegosAlmacenadas().size(); i++) {
                        
                            System.out.println("==>" +gst.getJuegosAlmacenadas().get(i).getTitulo());
                    }      
                            System.out.println("\n\n Entra el titulo del juego que quiere eliminar");
                            String titulo = sc.nextLine();
                            
                           boolean remove = false;
                            
                            for (int i = 0; i < gst.getJuegosAlmacenadas().size(); i++) {

                                if(gst.getJuegosAlmacenadas().get(i).getTitulo().equals(titulo)){
                                    gst.getJuegosAlmacenadas().remove(i);
                                   remove = true;
                                    //seria mejor hacer este etapa con un ID porque asi el programa no es eficiente...
                                   }
                                }   
                          
                          if(remove){
                            System.out.println("\n\n Juego : \"" + titulo +"\" eleminada con exit ! \n\n");
                            gst.creearFicheroXml(gst); //linea que crea el fichero xml
                            gst.xFormerXml(); //linea que transforma y almacena el fichero xml
                            System.out.println("\n\n Programa Finalizado!"); 
                          }else{
                              System.out.println("\n\n El juego \""+titulo + "\" no esta almacenado \n\n");
                              gst.menuSelect();
                          }
                            
                    break;
                }
            default:
                System.out.println("\n\n Programa Finalizado!");
                break;
        }
        
      
    }

    

    

    
}
