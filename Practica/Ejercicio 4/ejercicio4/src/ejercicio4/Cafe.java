package ejercicio4;
/**
 *
 * @author chris
 */
public class Cafe {
    
    private String marca;
    private String precio;
    private String ventas;
    private String total;

    public Cafe(String marca, String precio, String ventas, String total) {
        this.marca = marca;
        this.precio = precio;
        this.ventas = ventas;
        this.total = total;
    }
    
    public Cafe(){
        
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the precio
     */
    public String getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(String precio) {
        this.precio = precio;
    }

    /**
     * @return the ventas
     */
    public String getVentas() {
        return ventas;
    }

    /**
     * @param ventas the ventas to set
     */
    public void setVentas(String ventas) {
        this.ventas = ventas;
    }

    /**
     * @return the total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(String total) {
        this.total = total;
    }
    
    
    
}
