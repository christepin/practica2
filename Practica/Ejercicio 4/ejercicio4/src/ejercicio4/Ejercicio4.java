package ejercicio4;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Ejercicio4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       ArrayList<Cafe> listCafe = new ArrayList<>();
       
       Cafe cafe1 = new Cafe("CafeIESSanFer", "6.8", "45", "500");
       Cafe cafe2 = new Cafe("CafeIESSanFer", "5.3", "43", "500");
       listCafe.add(cafe2);
       listCafe.add(cafe1);
       
       Proveedor p = new Proveedor("Mi Calle", "Madrid", "España", "28050", "Importation",listCafe);
       p.setCif("150");
        
        XStream xstream = new XStream(new DomDriver());
        
       xstream.alias("proveedor", p.getClass()); //Personalzar mi etiquetta raiz al nombre proveedor.
       xstream.useAttributeFor(p.getClass(), "cif"); //Añadir como id mi attributo de class "cif"
        
       xstream.addImplicitArray(p.getClass(), "cafeList"); //especificar que quiero que mis cafe se ve como incluido a mi raiz proveedor..
       
       xstream.alias("cafe", ejercicio4.Cafe.class);

       
       
       
      
        
        String xml = xstream.toXML(p);     
                
        System.out.print(xml);
        
      
        Proveedor copia = (Proveedor) xstream.fromXML(xstream.toXML(p));
       
        System.out.println("\n\n\n  --->>>>>Recuperation del objecto<<<<<--- ");
         
        
    
        

       
    }
    
}
