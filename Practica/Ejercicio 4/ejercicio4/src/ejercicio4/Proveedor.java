
package ejercicio4;

import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Proveedor {
    
    
 
  private String calle;
  private String cuidad;
  private String pais;
  private String cp;
  private String esNacional;
  private String cif;
  private  ArrayList<Cafe> cafeList;
  
    public Proveedor(String calle, String cuidad, String pais, String cp, String esNacional,ArrayList<Cafe> cafeList) {
        this.cafeList = cafeList;
        this.calle = calle;
        this.cuidad = cuidad;
        this.pais = pais;
        this.cp = cp;
        this.esNacional = esNacional;
    }
    
    public Proveedor(){
        
    }

    /**
     * @return the cafeList
     */
    public ArrayList<Cafe> getCafeList() {
        return cafeList;
    }

    /**
     * @param cafeList the cafeList to set
     */
    public void setCafeList(ArrayList<Cafe> cafeList) {
        this.cafeList = cafeList;
    }

    /**
     * @return the calle
     */
    public String getCalle() {
        return calle;
    }

    /**
     * @param calle the calle to set
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * @return the cuidad
     */
    public String getCuidad() {
        return cuidad;
    }

    /**
     * @param cuidad the cuidad to set
     */
    public void setCuidad(String cuidad) {
        this.cuidad = cuidad;
    }

    /**
     * @return the pais
     */
    public String getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(String pais) {
        this.pais = pais;
    }

    /**
     * @return the cp
     */
    public String getCp() {
        return cp;
    }

    /**
     * @param cp the cp to set
     */
    public void setCp(String cp) {
        this.cp = cp;
    }

    /**
     * @return the esNacional
     */
    public String getEsNacional() {
        return esNacional;
    }

    /**
     * @param esNacional the esNacional to set
     */
    public void setEsNacional(String esNacional) {
        this.esNacional = esNacional;
    }

    /**
     * @return the cif
     */
    public String getCif() {
        return cif;
    }

    /**
     * @param cif the cif to set
     */
    public void setCif(String cif) {
        this.cif = cif;
    }
  
  
  
}
