/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio5;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author chris
 */
public class Alumno {
    
    private String nombre;
    private String apellido;
    private String fecha;
   
//    private String[] domicilio = new String[2];
    
    private ArrayList<String> domicilio = new ArrayList<String>();

    public Alumno() {
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Enter tu Nombre :");
        this.nombre = teclado.nextLine();
        System.out.println("Enter tu apellido : ");
        this.apellido = teclado.nextLine();
        System.out.println("Enter tu fecha de Nacimiento : ");
        this.fecha = teclado.nextLine();
        System.out.println("Enter tu calle : ");
      
        this.domicilio.add(teclado.nextLine());
        System.out.println("Enter numero de la puerta : ");
    
        this.domicilio.add(teclado.nextLine());
        
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    /**
     * @return the domicilio
     */
    public ArrayList<String> getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(ArrayList<String> domicilio) {
        this.domicilio = domicilio;
    }


    
    
    
    
}
