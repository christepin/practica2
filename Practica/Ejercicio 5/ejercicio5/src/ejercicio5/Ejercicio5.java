
package ejercicio5;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Ejercicio5 {
    
     
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         
        
       XStream xstream = new XStream(new DomDriver());
       
       ArrayList<Alumno> aula = new ArrayList<>();
       Alumno a = new Alumno();
       Alumno a2 = new Alumno();
       aula.add(a);
       aula.add(a2);
        
        xstream.alias("alumno", a.getClass());
        
        xstream.alias("aula", aula.getClass());
       
       
        String xml = xstream.toXML(aula);     
           
     
         
         System.out.print(xml);
         
         ArrayList<Alumno>  copia = (ArrayList<Alumno>) xstream.fromXML(xml);
      
        System.out.println("\n\n\n-------->>>>Recuperation objecto<<<<---------");
        
        System.out.println("\n\n");
        
          
        XStream xstream2 = new XStream(new JsonHierarchicalStreamDriver()); //JSON------------------------
        xstream2.setMode(XStream.NO_REFERENCES);
      
        xstream2.alias("Alumno", a.getClass());
        xstream2.alias("Alumno", a2.getClass());
        

        System.out.println(xstream2.toXML(a) + "\n\n" + xstream2.toXML(a2) );
         
        
    }
    
}
